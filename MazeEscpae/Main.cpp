#include <SFML/Graphics.hpp>
#include "AssetManager.h"
#include "Level.h"
int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Maze Escape", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;

	// Level
	Level levelObject;

	levelObject.LoadLevel(2);
	
	// Game Loop
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed

			levelObject.Input();

			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		} // End event polling loop

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		// Get the time passed since the last frame and restart our game clock

		sf::Time frameTime = gameClock.restart();

		// Player Update
		levelObject.Update(frameTime);

		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color(15, 15, 15));

		// Draw game objects

		// Draw Player
		levelObject.DrawTo(gameWindow);

		// Display the window contents on the screen
		gameWindow.display();

	} // End of Game Loop
	return 0;
}