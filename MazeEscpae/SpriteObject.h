#pragma once
#include <SFML/Graphics.hpp>
class SpriteObject
{
public:
	// Constructors / Destructors
	SpriteObject(sf::Texture& newTexture);
	// Functions
	void DrawTo(sf::RenderTarget& target);

	// Getters
	sf::FloatRect GetHitBox();

	// Setters
	void SetPosition(sf::Vector2f newPosition);

protected:
	sf::Vector2f CalculateCollisionDepth(sf::FloatRect otherHitbox);

	// Data
	sf::Sprite sprite;
};

