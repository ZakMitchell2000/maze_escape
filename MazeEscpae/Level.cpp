#include "Level.h"
#include <iostream>
#include <fstream>

Level::Level()
	: playerInstance(sf::Vector2f(0, 0))
	, wallInstances()
{
	wallInstances.push_back(Wall(sf::Vector2f(200, 200)));
	wallInstances.push_back(Wall(sf::Vector2f(300, 200)));
	wallInstances.push_back(Wall(sf::Vector2f(400, 200)));

}

void Level::Input()
{
	playerInstance.Input();
}

void Level::Update(sf::Time frameTime)
{
	playerInstance.Update(frameTime);

	for (int i = 0; i < wallInstances.size(); ++i)
	{
		playerInstance.HandleSolidCollision(wallInstances[i].GetHitBox());
	}

}

void Level::DrawTo(sf::RenderTarget& target)
{
	playerInstance.DrawTo(target);

	for (int i = 0; i < wallInstances.size(); ++i)
	{
		wallInstances[i].DrawTo(target);
	}
}

void Level::LoadLevel(int levelNumber)
{
	// Figure out the file path based on the level number
	std::string filePath = "Assets/Levels/Level" + std::to_string(levelNumber) + ".txt";

	// Open the level file using this path
	std::ifstream inFile;
	inFile.open(filePath);
	// TODO: use the file

	if (!inFile)
	{
		std::cerr << "Unable to open file " + filePath;
		exit(1); // Call system to stop program with error
	}

	// Clear out the existing level to be ready to read the new one
	wallInstances.clear();

	// Read through the file, processing:
	char ch;
	// Set the starting x and y coordinates used to position level objects
	float x = 0.0f;
	float y = 0.0f;
	// Define the spacing we will use for our grid
	float xSpacing = 100.0f;
	float ySpacing = 100.0f;
	while (inFile >> std::noskipws >> ch)
	{
		if (ch == ' ')
		{
			// New column, increase x
			x += xSpacing;
		}
		else if (ch == '\n')
		{
			// New row, increase y and set x back to 0
			y += ySpacing;
			x = 0;
		}
		else if (ch == 'P')
		{
			// Player (positioning the player)
			playerInstance.SetPosition(sf::Vector2f(x, y));
		}
		else if (ch == 'W')
		{
			// Walls (creating them, positioning them, and adding them to the vector)
			wallInstances.push_back(Wall(sf::Vector2f(x, y)));
		}

		else if (ch == '-')
		{
			// Do nothing - empty space
		}
		else
		{
			std::cerr << "Unrecognised character in level file: " << ch;
		}
	}


	// Close the file now that we are done with it
	inFile.close();


}