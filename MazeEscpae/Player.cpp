#include "Player.h"
#include "AssetManager.h"
#include <cmath>
Player::Player(sf::Vector2f startingPos)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"), 100, 100, 5.0f)
		, velocity(0.0f, 0.0f)
		, speed(300.0f)
		, previousPosition(startingPos)
{
	sprite.setPosition(startingPos);

	AddClip("walkDown", 0, 3);
	AddClip("walkRight", 4, 7);
	AddClip("walkUp", 8, 11);
	AddClip("walkLeft", 12, 15);

	PlayClip("walkDown");
}
void Player::Input()
{
	// Player keybind input
	// Start by zeroing out player velocity
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	bool hasInput = false;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
		if (!hasInput)
			PlayClip("walkUp");
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
		if (!hasInput)
			PlayClip("walkDown");
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
		if(!hasInput)
			PlayClip("walkLeft");
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
		if (!hasInput)
			PlayClip("walkRight");
		hasInput = true;
	}
	if (!hasInput)
	{
		Stop();
	}
}
void Player::Update(sf::Time frameTime)
{
	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity*frameTime.asSeconds();
	// Move the player to the new position
	previousPosition = sprite.getPosition();

	sprite.setPosition(newPosition);

	AnimatingObject::Update(frameTime);
}
void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	// Check if there is actually a collision happening
	bool isColliding = GetHitBox().intersects(otherHitbox);
	// If there is a collision...
	if (isColliding)
	{
		// Calculate the collision depth (overlap)
		sf::Vector2f depth = CalculateCollisionDepth(otherHitbox);

		// Determine which is smaller - the x or y overlap
		// Determine which is smaller - the x or y overlap
		sf::Vector2f newPosition = sprite.getPosition();
		if (std::abs(depth.x) < std::abs(depth.y))
		{
			// Calculate a new x coordinate such that the objects don't overlap
				newPosition.x -= depth.x;
		}
		else
		{
			// Calculate a new y coordinate such that the objects don't overlap
				newPosition.y -= depth.y;
		}

		// Move the sprite by the depth in whatever direction was smaller
		sprite.setPosition(newPosition);
	}
}


