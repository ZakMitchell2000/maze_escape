#include "AnimatingObject.h"
AnimatingObject::AnimatingObject(sf::Texture& newTexture, int newFrameWidth, int newFrameHeight, float newFPS)
	: SpriteObject(newTexture)
	, frameWidth(newFrameWidth)
	, frameHeight(newFrameHeight)
	, framesPerSecond(newFPS)
	, currentFrame(0)
	, timeInFrame(sf::seconds(0.0f))
	, clips()
	, currentClip("")
	, playing(false)
{
}
void AnimatingObject::Update(sf::Time frameTime)
{
	if (!playing)
		return;
	// If it is time for a new frame...
	timeInFrame += frameTime;
	sf::Time timePerFrame = sf::seconds(1.0f / framesPerSecond);
	if (timeInFrame >= timePerFrame)
	{
		// Update the current frame
		++currentFrame;
		timeInFrame = sf::seconds(0);

		// If we got to the end of the clip...
		Clip& thisClip = clips[currentClip];
		if (currentFrame > thisClip.endFrame)
		{
			// Return to the beginning of the clip
			currentFrame = thisClip.startFrame;
		}

		UpdateSpriteTextureRect();

	}
}
void AnimatingObject::AddClip(std::string name, int startFrame,int endFrame)
{
	// Create new animation
	Clip& newClip = clips[name];
	// Setup the settings for this animation
	newClip.startFrame = startFrame;
	newClip.endFrame = endFrame;
}
void AnimatingObject::PlayClip(std::string name)
{
	// If the clip trying to play is playing
	// Exit
	if (currentClip == name)
	{
		return;
	}

	// Find the clip's information in the map
	auto pairFound = clips.find(name);
	// If the clip exists...
	if (pairFound != clips.end())
	{
		// Set up the animation based on the clip
		currentClip = name;
		currentFrame = pairFound->second.startFrame;
		timeInFrame = sf::seconds(0.0f);
		playing = true;
		// Update sprite rect
		UpdateSpriteTextureRect();
	}
}
void AnimatingObject::UpdateSpriteTextureRect()
{
	int numFramesX = sprite.getTexture()->getSize().x / frameWidth;
	int xFrameIndex = currentFrame % numFramesX;
	int yFrameIndex = currentFrame / numFramesX;
	sf::IntRect textureRect;
	textureRect.left = xFrameIndex * frameWidth;
	textureRect.top = yFrameIndex * frameHeight;
	textureRect.width = frameWidth;
	textureRect.height = frameHeight;
	sprite.setTextureRect(textureRect);
}
void AnimatingObject::Pause()
{
	playing = false;
}
void AnimatingObject::Stop()
{
	playing = false;
	// Reset to first frame of the animation
	Clip& thisClip = clips[currentClip];
	currentFrame = thisClip.startFrame;
	UpdateSpriteTextureRect();
}
void AnimatingObject::Resume()
{
	if (!currentClip.empty())
		playing = true;
}

